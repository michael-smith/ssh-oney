# ssh-oney
A small containerized ssh honeypot to capture credentials that were tried for ssh login.

## Quickstart
### Setup
- Docker or podman is available at the machine
- Port 22 is available and not used by the real ssh server (e.g. for openssh-server in `/etc/ssh/sshd_config` change the line `#Port 22` to e.g. `Port 123`)
- When using podman:
  - port 22 can be used by non-root users
  OR
  - port 22 is forwarded to an unprivileged port, e.g. 2222. Then also change in `docker-compose.override.yml` the line `"22:22"` to `"2222:22"`.
  - `loginctl enable-linger`, that the containers do not stop when the user logs out
  - `systemctl --user start podman-restart.service` and `systemctl --user enable podman-restart.service`, that the restart policy works
- Clone this repository to your machine
- `cp .env.example .env`
- When more information about the IP addresses should be gathered: create account at https://ipdata.co/
- Modify the file .env to set the passwords (and ipdata api key)
- Build the containers with `podman-compose build`
- When the api key is avaiable, start the honeypot with

        podman-compose up -d

    otherwise

        podman-compose up -d ssh db

- The machines honeypot host keys are located in `bind-mount/ssh-host_key/`
- The logfiles are located at `bind-mount/log/`

### Login attempts
To see the tried logins, run

    podman exec -it ssh-oney_db_1 psql -U ssh

and execute the sql command for the tried credentials

    SELECT * FROM attempt ORDER BY ip_id DESC;

or for the ip addresses that have tried

    SELECT * FROM ip ORDER BY id DESC;
