CREATE DATABASE ssh;

\c ssh;

CREATE TABLE ip (
	id SERIAL PRIMARY KEY,
	address VARCHAR(45),
	service TEXT,
	latitude DOUBLE PRECISION,
	longitude DOUBLE PRECISION,
	continent TEXT,
	country TEXT,
	city TEXT,
	asn TEXT,
	checked BOOLEAN);

CREATE TABLE attempt (
	username TEXT,
	password TEXT,
	time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	ip_id INT,
	CONSTRAINT fk_ip FOREIGN KEY (ip_id) REFERENCES ip (id));

CREATE USER ssh;
GRANT SELECT, INSERT ON ip, attempt TO ssh;
GRANT USAGE, SELECT ON SEQUENCE ip_id_seq TO ssh;

CREATE USER updater;
GRANT SELECT, UPDATE ON ip TO updater;

CREATE USER backend;
GRANT SELECT ON ip TO backend;
