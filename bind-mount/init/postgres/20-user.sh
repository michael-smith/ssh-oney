#!/bin/bash

psql -U postgres -c "ALTER USER ssh PASSWORD '$POSTGRES_SSH_PASSWORD';"
psql -U postgres -c "ALTER USER updater PASSWORD '$POSTGRES_UPDATER_PASSWORD';"
psql -U postgres -c "ALTER USER backend PASSWORD '$POSTGRES_BACKEND_PASSWORD';"
