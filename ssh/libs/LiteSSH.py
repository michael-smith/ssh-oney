import io
import random
import base64
import hashlib
import hmac
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.asymmetric import x25519                   # curve25519-sha256 (RFC8731), shared secret for ephemeral keys
from cryptography.hazmat.primitives.asymmetric import ed25519                  # signing algorithm for host_key
from cryptography.hazmat.primitives import serialization


SSH_MSG_DISCONNECT = 1
SSH_MSG_SERVICE_REQUEST = 5
SSH_MSG_SERVICE_ACCEPT = 6
SSH_MSG_KEXINIT = 20
SSH_MSG_NEWKEYS = 21
SSH_MSG_KEX_ECDH_INIT = 30
SSH_MSG_KEX_ECDH_REPLY = 31
SSH_MSG_USERAUTH_REQUEST = 50
SSH_MSG_USERAUTH_FAILURE = 51

DEFAULT_MESSAGE_LIST = ["packet_length",    # uint32
                        "padding_length",   # byte
                        "payload",          # byte[n1]; n1 = packet_length - padding_length - 1
                        "random_padding",   # byte[n2]; n2 = padding_length
                        "mac"]              # byte[m]; m = mac_length

KEX_PAYLOAD_LIST = ["SSH_MSG_KEXINIT",                          # byte
                    "cookie",                                   # byte[16]; random bytes
                    "kex_algorithms",                           # name-list
                    "server_host_key_algorithms",               # name-list
                    "encryption_algorithms_client_to_server",   # name-list
                    "encryption_algorithms_server_to_client",   # name-list
                    "mac_algorithms_client_to_server",          # name-list
                    "mac_algorithms_server_to_client",          # name-list
                    "compression_algorithms_client_to_server",  # name-list
                    "compression_algorithms_server_to_client",  # name-list
                    "languages_client_to_server",               # name-list
                    "languages_server_to_client",               # name-list
                    "first_kex_packet_follows",                 # boolean
                    "reserved"]                                 # uint32; 0 (reserved for future extensions)

SERVER_INIT_KEX_DATA = {}
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[0]] = int.to_bytes(SSH_MSG_KEXINIT, 1, "big")
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[1]] = random.randbytes(16)
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[2]] = b"curve25519-sha256,curve25519-sha256@libssh.org"   # REQUIRED missing: diffie-hellman-group1-sha1, diffie-hellman-group14-sha1
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[3]] = b"ssh-ed25519"                                      # REQUIRED missing: ssh-dss
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[4]] = b"aes256-ctr"                                       # REQUIRED missing: 3des-cbc
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[5]] = SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[4]]
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[6]] = b"hmac-sha2-256,hmac-sha1"                          # REQUIRED present: hmac-sha1
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[7]] = SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[6]]
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[8]] = b"none"                                             # REQUIRED present: none
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[9]] = SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[8]]
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[10]] = b""
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[11]] = SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[10]]
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[12]] = b"\x00"
SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[13]] = b"\x00\x00\x00\x00"


def string(s):
    return int.to_bytes(len(s), 4, "big") + s

def get_string(stream):
    length = int.from_bytes(stream.read(4), "big")
    return stream.read(length), stream

def mpint(n):
    if isinstance(n, bytes):
        n = int.from_bytes(n, "big")
    if not n:
        return b"\x00" * 4

    n_bytes_length = (len(bin(n)) - 2) / 8
    if n_bytes_length == int(n_bytes_length):
        n_bytes_length = int(n_bytes_length)
    else:
        n_bytes_length = int(n_bytes_length) + 1

    b = int.to_bytes(n, n_bytes_length, "big")
    if int.from_bytes(b[:1], "big") & 128:
        b = b"\x00" + b
    return string(b)


class LiteSSH:
    def __init__(self, conn, addr, name="OpenSSH_7.9p1"):
        self.conn = conn
        self.addr = addr
        self.server_name = "SSH-2.0-{}\r\n".format(name).encode("US-ASCII")
        self.sequence_number_client = -1
        self.sequence_number_server = -1

    # Misc ====================================================================
    def get_matching_algorithm(self, key):
        for item in self.client_init_kex_data[key].split(b","):
            if item in SERVER_INIT_KEX_DATA[key].split(b","):
                return item
        raise RuntimeError("get_matching_algorithm: No shared algorithm found. Expected '{}' but got '{}'".format(SERVER_INIT_KEX_DATA[key],
                                                                                                                  self.client_init_kex_data[key]))

    def compute_keys(self, char):
        if self.kex_key_name in [b'curve25519-sha256', b'curve25519-sha256@libssh.org']:
            data = mpint(self.shared_secret) + \
                self.session_id + \
                char.encode("US-ASCII") + \
                self.session_id
            return hashlib.sha256(data).digest()
        else:
            raise RuntimeError("compute_keys: Unknown key exchange algorithm")

    def create_mac(self, message):
        if self.hmac_alg_name_server == b"hmac-sha2-256":
            hmac_alg = hashlib.sha256
            key_length = 32
        elif self.hmac_alg_name_server == b"hmac-sha1":
            hmac_alg = hashlib.sha1
            key_length = 20

        return hmac.new(self.integrity_key_server[:key_length],
                        int.to_bytes(self.sequence_number_server, 4, "big") +
                            message,
                        hmac_alg).digest()
    # =========================================================================

    # Connection ==============================================================
    def recv(self, n, count_seq_num=True):
        if count_seq_num:
            self.sequence_number_client += 1
        return self.conn.recv(n)

    def send(self, packet):
        self.sequence_number_server += 1
        self.conn.send(packet)
    # =========================================================================

    # Unencrypted packet ======================================================
    def recv_unencrypted_packet(self, msg_code=None):
        packet_length = int.from_bytes(self.recv(4, False), "big")
        packet = self.recv(packet_length)
        payload = self.get_packet_payload(packet, msg_code)
        return payload

    def get_packet_payload(self, packet, msg_code):
        stream = io.BytesIO(packet)
        packet_length = len(packet)
        padding_length = int.from_bytes(stream.read(1), "big")
        n1 = packet_length - padding_length - 1
        payload = stream.read(n1)

        if msg_code:
            msg_code_payload = int.from_bytes(payload[:1], "big")
            if msg_code_payload == msg_code:
                return payload[1:]
            else:
                raise RuntimeError("get_packet_payload: Wrong message code. Expected '{}' but got '{}' in payload '{}'".format(msg_code,
                                                                                                                               msg_code_payload,
                                                                                                                               payload))
        else:
            return payload

    def create_unencrypted_packet(self, payload, bs):
        bs = 8 if bs < 8 else bs
        padding_length = bs - ((5 + len(payload)) % bs)
        padding_length = padding_length if padding_length >= 4 else padding_length + bs
        random_padding = random.randbytes(padding_length)
        data = int.to_bytes(padding_length, 1, "big") + payload + random_padding
        return string(data)

    def send_unencrypted(self, payload, bs=0):
        packet = self.create_unencrypted_packet(payload, bs)
        self.send(packet)
    # =========================================================================

    # Encrypted packet ========================================================
    def recv_encrypted_packet(self, msg_code=None):
        packet_length_enc = self.recv(4, False)
        packet_length = self.client_decryptor.update(packet_length_enc)
        packet_length = int.from_bytes(packet_length, "big")
        packet_enc = self.recv(packet_length, False)
        packet = self.client_decryptor.update(packet_enc)
        payload = self.get_packet_payload(packet, msg_code)

        if self.hmac_alg_name_client == b"hmac-sha2-256":
            length = 32
            hmac_alg = hashlib.sha256
        elif self.hmac_alg_name_client == b"hmac-sha1":
            length = 20
            hmac_alg = hashlib.sha1

        mac = self.recv(length)
        mac_check = hmac.new(self.integrity_key_client[:length],
                             int.to_bytes(self.sequence_number_client, 4, "big") +
                                 int.to_bytes(packet_length, 4, "big") + packet,
                             hmac_alg).digest()

        if not mac == mac_check:
            raise RuntimeError("recv_encrypted_packet: Wrong MAC received")

        return payload

    def create_encrypted_packet(self, payload):
        data = self.create_unencrypted_packet(payload, 16)  # hard coded bs aes-256
        mac = self.create_mac(data)
        return self.server_encryptor.update(data) + mac

    def send_encrypted(self, payload):
        packet = self.create_encrypted_packet(payload)
        self.send(packet)
    # =========================================================================

    # Kex =====================================================================
    def get_kex_init_payload(self, payload):
        stream = io.BytesIO(payload)

        client_init_kex_data = {}
        client_init_kex_data["SSH_MSG_KEXINIT"] = stream.read(1)
        if int.from_bytes(client_init_kex_data["SSH_MSG_KEXINIT"], "big") != SSH_MSG_KEXINIT:
            raise RuntimeError("get_kex_init_payload: Wrong message code. Expected '{}' but got '{}' in payload '{}'".format(SSH_MSG_KEXINIT,
                                                                                                                             int.from_bytes(client_init_kex_data["SSH_MSG_KEXINIT"], "big"),
                                                                                                                             payload))
        client_init_kex_data["cookie"] = stream.read(16)
        for key in KEX_PAYLOAD_LIST[2:-2]:
            client_init_kex_data[key], stream = get_string(stream)
        client_init_kex_data["first_kex_packet_follows"] = stream.read(1)
        client_init_kex_data["reserved"] = stream.read(4)
        return client_init_kex_data

    def create_kex_init_payload(self):
        data = SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[0]] + SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[1]]
        for key in KEX_PAYLOAD_LIST[2:-2]:
            data += string(SERVER_INIT_KEX_DATA[key])
        kex_payload = data + SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[12]] + SERVER_INIT_KEX_DATA[KEX_PAYLOAD_LIST[13]]
        return kex_payload

    def recv_pub_key(self):
        if self.kex_key_name in [b'curve25519-sha256', b'curve25519-sha256@libssh.org']:
            payload = self.recv_unencrypted_packet(SSH_MSG_KEX_ECDH_INIT)
            stream = io.BytesIO(payload)
            client_pub_key_ecdh, stream = get_string(stream)
            return client_pub_key_ecdh, self.load_x25519_pub_key_from_bytes(client_pub_key_ecdh)

        else:
            raise RuntimeError("recv_pub_key: Unknown key exchange algorithm")

    def create_key_exchange_reply(self):
        if self.kex_key_name in [b'curve25519-sha256', b'curve25519-sha256@libssh.org']:
            # Server host key, ed25519
            server_host_key_priv_full = self.get_ed25519_server_host_key_priv_full()
            self.server_host_key_priv = self.load_ed25519_priv_key_from_bytes(server_host_key_priv_full)

            self.server_host_key_pub_full = self.get_ed25519_server_host_key_pub_full()             # part 1/3

            # Ephemeral keys, x25519
            self.server_priv_key = self.create_x25519_server_priv_key()
            # self.server_priv_key_ecdh = self.get_ex25519_priv_key_ecdh(self.server_priv_key)
            self.server_pub_key = self.server_priv_key.public_key()
            self.server_pub_key_ecdh = self.get_ex25519_pub_key_ecdh(self.server_pub_key)           # part 2/3
            self.shared_secret = self.server_priv_key.exchange(self.client_pub_key)

            exchange_hash = self.create_ed25519_exchange_hash()
            self.session_id = exchange_hash
            sig_exchange_hash = self.server_host_key_priv.sign(exchange_hash)                       # part 3/3
            name = b"ssh-ed25519"
            kex_h_signature = string(name) + string(sig_exchange_hash)

            return int.to_bytes(SSH_MSG_KEX_ECDH_REPLY, 1, "big") + \
                string(self.server_host_key_pub_full) + \
                string(self.server_pub_key_ecdh) + \
                string(kex_h_signature)

        else:
            raise RuntimeError("create_key_exchange_reply: Unknown key exchange algorithm")

    def send_new_keys(self):
        payload = int.to_bytes(SSH_MSG_NEWKEYS, 1, "big")
        self.send_unencrypted(payload)
    # =========================================================================

    # Ed25519 =================================================================
    def get_ed25519_server_host_key_priv_full(self):
        with open("host_keys/ssh_host_ed25519_key", "r") as f:
            data = f.readlines()
        server_host_key_full = ""
        for line in data:
            if not line.startswith("-"):
                server_host_key_full += line.replace("\n", "")
        server_host_key_full = base64.b64decode(server_host_key_full)
        stream = io.BytesIO(server_host_key_full.split(b"ssh-ed25519")[-1])
        public_key_length = int.from_bytes(stream.read(4), "big")
        public_key = stream.read(public_key_length)
        private_key_length = int.from_bytes(stream.read(4), "big") - public_key_length
        private_key = stream.read(private_key_length)
        return private_key

    def get_ed25519_server_host_key_pub_full(self):
        with open("host_keys/ssh_host_ed25519_key.pub", "r") as f:
            data = f.read()
        server_host_key_pub_full = data.split(" ")[1]
        return base64.b64decode(server_host_key_pub_full)

    def create_x25519_server_priv_key(self):
        private_key = x25519.X25519PrivateKey.generate()
        return private_key

    def get_ex25519_priv_key_ecdh(self, private_key):
        return private_key.private_bytes(
            serialization.Encoding.Raw,
            serialization.PrivateFormat.Raw,
            serialization.NoEncryption()
            )

    def get_ex25519_pub_key_ecdh(self, public_key):
        return public_key.public_bytes(
            serialization.Encoding.Raw,
            serialization.PublicFormat.Raw,
            )

    def load_ed25519_priv_key_from_bytes(self, private_key):
        return ed25519.Ed25519PrivateKey.from_private_bytes(private_key)

    def load_x25519_pub_key_from_bytes(self, public_key):
        return x25519.X25519PublicKey.from_public_bytes(public_key)

    def create_ed25519_exchange_hash(self):
        data = string(self.client_name.replace(b"\r\n", b"")) + \
            string(self.server_name.replace(b"\r\n", b"")) + \
            string(self.client_kex_payload) + \
            string(self.server_kex_payload) + \
            string(self.server_host_key_pub_full) + \
            string(self.client_pub_key_ecdh) + \
            string(self.server_pub_key_ecdh) + \
            mpint(self.shared_secret)
        return hashlib.sha256(data).digest()
    # =========================================================================

    # Encryption ==============================================================
    def create_cipher(self, name, key, iv):
        if name == b"aes256-ctr":
            return Cipher(algorithms.AES(key), modes.CTR(iv))
    # =========================================================================

    def init(self):
        data = self.recv(1024, False)
        if data[:5] == b"SSH-2" and data[-2:] == b"\r\n":
            self.client_name = data
            self.send(self.server_name)
        else:
            raise RuntimeError("init: No SSH-2 init. Received '{}'".format(data))

    def init_kex(self):
        self.client_kex_payload = self.recv_unencrypted_packet()
        self.client_init_kex_data = self.get_kex_init_payload(self.client_kex_payload)
        self.server_kex_payload = self.create_kex_init_payload()
        self.send_unencrypted(self.server_kex_payload)

    def key_exchange(self):
        self.kex_key_name = self.get_matching_algorithm("kex_algorithms")
        cipher_name_client = self.get_matching_algorithm("encryption_algorithms_client_to_server")
        cipher_name_server = self.get_matching_algorithm("encryption_algorithms_server_to_client")
        self.hmac_alg_name_client = self.get_matching_algorithm("mac_algorithms_client_to_server")
        self.hmac_alg_name_server = self.get_matching_algorithm("mac_algorithms_server_to_client")

        self.client_pub_key_ecdh, self.client_pub_key = self.recv_pub_key()
        server_key_exchange_reply = self.create_key_exchange_reply()
        self.send_unencrypted(server_key_exchange_reply)

        iv_client = self.compute_keys("A")[:16]
        iv_server = self.compute_keys("B")[:16]
        enc_key_client = self.compute_keys("C")
        enc_key_server = self.compute_keys("D")
        self.integrity_key_client = self.compute_keys("E")
        self.integrity_key_server = self.compute_keys("F")
        cipher_client = self.create_cipher(cipher_name_client, enc_key_client, iv_client)
        cipher_server = self.create_cipher(cipher_name_server, enc_key_server, iv_server)

        self.client_decryptor = cipher_client.decryptor()
        self.server_encryptor = cipher_server.encryptor()

        self.send_new_keys()
        self.recv_unencrypted_packet(SSH_MSG_NEWKEYS)

    def userauth(self):
        stream = io.BytesIO(self.recv_encrypted_packet(SSH_MSG_SERVICE_REQUEST))
        service_name, stream = get_string(stream)
        if not service_name == b"ssh-userauth":
            raise RuntimeError("userauth: Wrong service name. Expected '{}' but got '{}'".format(b"ssh-userauth", service_name))
        payload = int.to_bytes(SSH_MSG_SERVICE_ACCEPT, 1, "big") + \
                  string(service_name)
        self.send_encrypted(payload)

    def service_auth(self, auth_none=True):
        stream = io.BytesIO(self.recv_encrypted_packet(SSH_MSG_USERAUTH_REQUEST))

        username, stream = get_string(stream)
        service_name, stream = get_string(stream)
        auth_method, stream = get_string(stream)

        if auth_method == b"none":
            payload = int.to_bytes(SSH_MSG_USERAUTH_FAILURE, 1, "big") + \
                      string(b"password") + \
                      b"\x00"
            self.send_encrypted(payload)
            if auth_none:
                return self.service_auth(False)
            else:
                raise RuntimeError("service_auth: Auth method should not be 'none' twice")
        elif auth_method == b"password":
            change_pw = int.from_bytes(stream.read(1), "big")
            password, stream = get_string(stream)
            payload = int.to_bytes(SSH_MSG_DISCONNECT, 1, "big") + \
                      int.to_bytes(1, 4, "big") + \
                      string(b"")
            self.send_encrypted(payload)
            return username, password, self.client_name
