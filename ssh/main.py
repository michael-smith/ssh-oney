#!/usr/bin/env python3

import socket
from libs.LiteSSH import LiteSSH
from threading import Thread
from queue import Queue

import psycopg2
import os
import time


class SSHserver:
    def __init__(self, ip, port, db_cur):
        self.ip = ip
        self.port = port
        self.q = Queue()
        self.q_error = Queue()
        self.db_cur = db_cur

        if not os.path.exists("host_keys/ssh_host_ed25519_key"):
            os.system('ssh-keygen -f host_keys/ssh_host_ed25519_key -t ed25519 -N ""')

    def serve(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.bind((self.ip, self.port))
        self.s.listen()

        Thread(target=self.read_queue).start()
        Thread(target=self.read_queue_error).start()

        while True:
            conn, addr = self.s.accept()
            conn.settimeout(5)
            Thread(target=self.capture_login, args=(conn, addr)).start()

    def capture_login(self, conn, addr):
        try:
            lssh = LiteSSH(conn, addr)
            lssh.init()
            lssh.init_kex()
            lssh.key_exchange()
            lssh.userauth()
            data = lssh.service_auth()
            conn.close()
            self.q.put((addr[0], data))
        except Exception as e:
            self.q.put((addr[0], None))
            self.q_error.put((addr[0], e))

    def read_queue(self):
        while True:
            address, data = self.q.get()
            if data:
                try:
                    username = data[0].decode("utf-8")
                    password = data[1].decode("utf-8")
                    service = data[2][:-2].decode("utf-8")
                except Exception as e:
                    with open("logs/decode_error.log", "a") as f:
                        f.write("{}\t{}\t{}\t{}\n".format(time.strftime("%Y-%m-%d %H:%M"), address, e, data))
                        f.flush()
                else:
                    self.write_db(address, username, password, service)
            else:
                self.write_db(address)

    def write_db(self, address, username=None, password=None, service=None):
        self.db_cur.execute("SELECT id FROM ip WHERE address = %s;",
                            (address,))
        ip_id = self.db_cur.fetchall()

        if ip_id == []:
            self.db_cur.execute("INSERT INTO ip (address, service, checked) VALUES (%s, %s, FALSE);",
                                (address, service))

        if username and password and service:
            if ip_id == []:
                self.db_cur.execute("SELECT id FROM ip WHERE address = %s;",
                                    (address,))
                ip_id = self.db_cur.fetchall()
            self.db_cur.execute("INSERT INTO attempt (username, password, ip_id) VALUES (%s, %s, %s);",
                                (username, password, ip_id[0][0]))

    def read_queue_error(self):
        while True:
            address, error = self.q_error.get()
            with open("logs/ssh_error.log", "a") as f:
                f.write("{}\t{}\t{}\n".format(time.strftime("%Y-%m-%d %H:%M"), address, error))
                f.flush()

    def close(self):
        self.s.close()


while True:
    try:
        db_conn = psycopg2.connect(user="ssh", password=os.environ.get("POSTGRES_SSH_PASSWORD"), database="ssh", host="db")
        break
    except Exception:
        time.sleep(2)
db_conn.autocommit = True
db_cur = db_conn.cursor()

ssh = SSHserver("0.0.0.0", 22, db_cur)
ssh.serve()
ssh.close()
