import requests
import psycopg2
import os
import time


class Updater:
    def __init__(self, api_key, db_cur):
        self.api_key = api_key
        self.db_cur = db_cur
        self.response_items = {"latitude": ["latitude"],
                               "longitude": ["longitude"],
                               "continent": ["continent_name"],
                               "country": ["country_name"],
                               "city": ["city"],
                               "asn": ["asn", "name"]}

    def get_unknown_ips(self):
        self.db_cur.execute("SELECT address FROM ip WHERE checked IS FALSE;")
        return self.db_cur.fetchall()

    def get_ip_info(self, ip):
        response = requests.get("https://api.ipdata.co/{}?api-key={}".format(ip, self.api_key))
        if response.status_code == 200:
            return response.json()
        else:
            # TODO thread for error log, write instead of raise
            raise RuntimeError("get_ip_info: Wrong status code. Expected '200' but got '{}' in '{}'".format(response.status_code,
                                                                                                            response.text))

    def write_db(self, ip, response):
        response_values = {}
        for item in self.response_items:
            key = self.response_items[item]
            try:
                value = response[key[0]] if len(key) == 1 else response[key[0]][key[1]]
            except KeyError:
                value = None
            response_values[item] = value
        self.db_cur.execute("UPDATE ip SET latitude = %s, longitude = %s, "
                            "continent = %s, country = %s, city = %s, "
                            "asn = %s, checked = TRUE WHERE address = %s;",
                            (response_values["latitude"], response_values["longitude"],
                             response_values["continent"], response_values["country"],
                             response_values["city"], response_values["asn"], ip))

    def update_db(self):
        ips = self.get_unknown_ips()
        for ip in ips:
            response = self.get_ip_info(ip[0])
            self.write_db(ip[0], response)

    def run(self):
        while True:
            try:
                self.update_db()
            except Exception as e:
                with open("logs/error.log", "a") as f:
                    f.write("{}\t{}\n".format(time.strftime("%Y-%m-%d %H:%M"), e))
                    f.flush()

            hour = time.strftime("%H")
            while hour == time.strftime("%H"):
                time.sleep(10)


while True:
    try:
        db_conn = psycopg2.connect(user="updater", password=os.environ.get("POSTGRES_UPDATER_PASSWORD"), database="ssh", host="db")
        break
    except Exception:
        time.sleep(2)
db_conn.autocommit = True
db_cur = db_conn.cursor()

updater = Updater(os.environ.get("IPDATA_API_KEY"), db_cur)
updater.run()
